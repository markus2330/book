\input{setup}

\date{2.3.2018}

\begin{document}

\renewcommand{\enquote}[1]{\emph{``#1''}} % Cannot be done earlier

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
	\titlepage
	\doclicenseThis
\end{frame}

\begin{frame}
	\frametitle{Language}
	\begin{itemize}
		\item Slides are in English
		\item Papers are in English
		\item Book is in English
	\end{itemize}
\end{frame}

\begin{assignment}
	\frametitle{Language of the Talk?}
	\begin{task}
	Hands up if you prefer German.
	\end{task}
	Unanimous preference of German required, otherwise English.
\end{assignment}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\section{Motivation}
{
\usebackgroundtemplate{\includegraphics[width=\paperwidth]{pics/clouds.jpg}}%
\begin{frame}
	\frametitle{Misconfiguration}
	\begin{itemize}
		\item configuration is a user interface for both developers and system administrators
		\item \empha[misconfiguration]{misconfigurations}~\cite{yin2011empirical,su2007autobash,attariyan2010automating,xu2015systems}
			are a major cause of system failures~\cite{wool2004quantitative,oppenheimer2003internet,pertet2005causes}
		\item much time needed to fix misconfigurations~\cite{rabkin2011static,oppenheimer2003internet,yin2011empirical,mahajan2002bgp}
	\end{itemize}
\end{frame}
\begin{frame}
	\frametitle{No-Futz}
	\begin{itemize}
		\item \citet{holland2001nofutz}~defined \empha{futzing} to denote \enquote{tinkering or fiddling experimentally with something.}
		\item With \intro[no-futz computing]{no-futz computing} \citet{holland2001nofutz} mean \enquote{that futzing should be allowed, but should never be required.}
		\item currently configuration is error-prone and under-specified, \empha{futzing} is often required
	\end{itemize}
\end{frame}
\begin{frame}
	\frametitle{Examples}
	Not every misconfiguration involves big companies, cloud, and huge amounts of money:
	\begin{itemize}
		\item No internet access because resolv.conf symlink broken.
		\item KDE crash because of ulimit setting.
		\item Out-of-service of computers during exam.
	\end{itemize}
\end{frame}
}
\begin{assignment}
	\frametitle{First Assignment}
	\begin{itemize}
		\item Have you already experienced misconfiguration?
		\item Did you read about misconfiguration in the news?
	\end{itemize}
	\begin{task}
	Discuss with your neighbor and tell us the best stories.
	\end{task}
\end{assignment}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\section{Content Overview}

\subsection{Terminology}
\begin{frame}
	\frametitle{Terminology}
	\input{../shared/def/configurationsetting.tex}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\subsection{Requirements}
\begin{frame}
\begin{restatable}{requirement}{reqLegacy}
A configuration library must be able to integrate (legacy) systems and must fully support (legacy) configuration files.%
\label{req:legacy}
\end{restatable}

\begin{restatable}{requirement}{reqValidate}
Validation of configuration settings must happen systematically before the application is even started.
\end{restatable}
\end{frame}

\begin{assignment}
	\frametitle{Requirements}
	\begin{task}
	Discuss about requirements a configuration framework should fulfil.
	\end{task}
\end{assignment}

\subsection{Topics}
\begin{frame}
	Topic: \textit{sources of configuration}
	\begin{itemize}
		\item semi-structured data
		\item configuration file formats
		\item command-line arguments
		\item environment variables
	\end{itemize}
\end{frame}

\begin{frame}
	Topic: \textit{design and architecture of configuration and configuration access}
	\begin{itemize}
		\item architectural decisions
		\item introspection
		\item code generation
		\item variability
	\end{itemize}
\end{frame}

\begin{frame}
	Topic: \textit{reduction of misconfiguration and configuration duplicates} \\
	Misconfiguration obviously needs configuration.
	We will discuss the ideas of:
	\begin{itemize}
		\item complexity reduction
		\item when configuration is needed (decisions, user interface)
		\item configuration-less systems (auto-detection)
		\item how duplication can be avoided (generation of artifacts)
		\item testability (generation of test cases)
	\end{itemize}
\end{frame}

\begin{frame}
	Further topics:
	\begin{itemize}
		\item \textit{context-awareness} (context-oriented programming)
		\item \textit{avoidance of dependences}
		\item \textit{cascading configuration}
		\item \textit{strategies for validation and modularization}
		\item \textit{documentation of configuration}
	\end{itemize}
\end{frame}

\begin{assignment}
	\begin{task}
	Break.
	\end{task}
\end{assignment}

\begin{frame}
	Topic: \textit{sound, complete and early detection of misconfiguration}
	\begin{itemize}
		\item points in time for configuration access and validation
		\item validation techniques
		\item constraints
	\end{itemize}
\end{frame}

\begin{frame}
	Topic: \textit{configuration as user interface}
	\begin{itemize}
		\item How system administrators work.
		\item Which user interfaces exist.
		\item How to specify configuration.
	\end{itemize}
\end{frame}

\begin{frame}
	Topic: \textit{desirable properties of configuration}
	\begin{itemize}
		\item self-description
		\item changeability
		\item idempotence
		\item round-tripping
	\end{itemize}
\end{frame}

\begin{frame}
	Topic: \textit{configuration management tools}
	\begin{itemize}
		\item Puppet
		\item CfEngine
		\item Nix
		\item others...
	\end{itemize}
\end{frame}


\begin{frame}
	\hspace*{-1cm}\includegraphics[width=\paperwidth]{dot/topics}
\end{frame}

\begin{frame}
	\frametitle{Elektra}
	\hfill \includegraphics[width=2cm]{../figures/logo}
	\vspace{-1cm}
	%Elektra is an implementation of the next generation of configuration systems:
	\begin{itemize}
		\item Elektra improves configuration management.
		\item Configuration management tools can use Elektra.
		\item Elektra implements what we discuss in this lecture.
		\item Elektra allows applications to fulfil the requirements we will discuss.
		\item Obviously you will also be able to apply the knowledge from this LVA without Elektra.
		\item Developed at TU Wien (\url{https://libelektra.org}).
	\end{itemize}
\end{frame}

\begin{assignment}
	\frametitle{In which topics are you interested?}
	\begin{task}[1]
	Choose a partner for this task.
	\end{task}

	\begin{task}[2]
	Go to stations and discuss topic with your partner.
	\end{task}

	\begin{task}[3]
	Write down the most interesting topics.
	(Can be topics of stations or new topics.)
	\end{task}
\end{assignment}





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\section{Organisation}

\subsection{Preliminaries}
\begin{frame}
	Communication:
	\begin{itemize}
		\item TISS (forum and news) \url{https://tiss.tuwien.ac.at/course/courseDetails.xhtml?courseNr=194030&semester=2018S}
		\item GitHub (private and public repo) \url{https://git.libelektra.org}
		\item EMail \url{markus.raab@complang.tuwien.ac.at}
		\item before/after/during lectures
	\end{itemize}
\end{frame}

\begin{assignment}
	\begin{task}
	Send me your GitHub name by email to get access to private repo.
	\end{task}
\end{assignment}

\begin{frame}
	Feedback:
	\begin{itemize}
		\item TISS anonymous feedback
		\item TISS LVA evaluation
		\item EMail \url{markus.raab@complang.tuwien.ac.at}
		\item before/after/during lectures
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Previous Knowledge}
	\begin{itemize}
		\item Obviously \textit{no} prior knowledge about Configuration or Configuration Management necessary.
		\item If you already have experience, you can use it in your talk and assignments.
		\item Knowledge about software engineering and software requirements is beneficially.
		\item You should have an understanding of large-scale software construction.
		\item Programming skills is a must.
	\end{itemize}
\end{frame}

\subsection{Grades}
\begin{frame}
	You will get a grade only if:
	\begin{itemize}
		\item You submitted your homework. (PR is not enough.)
		\item You participated in the team exercise.
		\item You gave your talk.
	\end{itemize}
\end{frame}

\begin{frame}
	To get a positive grade:
	\begin{itemize}
		\item All parts must be done.
		\item All parts must be positive.
	\end{itemize}
\end{frame}

\begin{frame}
	Grade is calculated from:
	\begin{description}
	\item[30\,\%:] homework
	\item[30\,\%:] team exercise
	\item[10\,\%:] talk
	\item[30\,\%:] test
	\item[+:] extrapoints
	\end{description}
\end{frame}

\subsection{Assignments}
\begin{frame}
	\frametitle{Talk}
	You can give a talk about anything related to configuration management.
	\begin{itemize}
		\item 20 minutes.
		\item It must be about your experience.
		\item I.e., not only about study of literature.
		\item It is okay if the experience happened during this LVA.
		\item If you extensively use some tool, please share your knowledge.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Home Work}
	You can select your own task related to \emph{use} configuration management.
	For example:
	\begin{itemize}
		\item Use Elektra or Puppet-Libelektra in/for a small application.
		\item Write a tutorial about how you did it.
		\item Fix usability bugs in Elektra, make it easier. (Extrapoints)
	\end{itemize}
\end{frame}

\begin{assignment}
	\begin{task}
	Think about a talk and homework till next week.
	Write it down in the private repo (first come, first served).
	\end{task}
\end{assignment}

\begin{frame}
	\frametitle{Team Exercise}
	You can select your team (2-3 people) and your task related to \emph{improve} configuration management.
	For example, you can improve Elektra:
	\begin{itemize}
		\item Write a validation plugin.
		\item Write support for a configuration file format.
		\item Make a configuration management tool use Elektra.
		\item Fix bugs. (Extrapoints)
	\end{itemize}
\end{frame}

\begin{assignment}
	\begin{task}
	Talk with someone who is not your neighbor about a potential collaboration in the team exercise.
	\end{task}
\end{assignment}

\begin{frame}
	Lecture is every week.
	Time Line:
	\begin{description}
		\item[9.3.2018:] TISS registration
		\item[16.3.2018:] topic homework and talk
		\item[23.3.2018:] teams found together
		\item[13.4.2018:] homework submitted, topics of team exercise
		\item[18.5.2018:] guest lecture
		\item[25.5.2018:] team exercise submitted
		\item[22.6.2018:] last corrections of team exercise
		\item[29.6.2018:] test
	\end{description}
\end{frame}

\begin{frame}
	\frametitle{Guest Lecture?}
	\begin{description}
		\item[Title:] Formal Foundations of Configuration Management for Program Analysis
		\item[Name:] Jürgen Cito, MIT
		\item[Date/Time:] 18.05.2018, 14:00 (c.t.), 1h
	\end{description}
\end{frame}

\begin{assignment}
	\frametitle{Questions?}
	\begin{task}
	Please read TISS and register for the course.
	\end{task}

	\begin{task}
	Any questions?
	\end{task}
\end{assignment}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\nocite{raab2017introducing}

\appendix

\begin{frame}[allowframebreaks]
	\bibliographystyle{plainnat}
	\bibliography{../shared/elektra.bib}
\end{frame}

\end{document}


